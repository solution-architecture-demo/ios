//
//  LoginViewController.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 2/27/22.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    let viewModel: LoginViewModel
    
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "LoginViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func didPressLogin(_ sender: UIButton) {
        let email = (emailTextField.text  ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        let password = (passwordTextField.text  ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        viewModel.login(with: .init(email: email, password: password)) { [weak self] isSuccess in
            DispatchQueue.main.async {
                guard isSuccess else {
                    self?.showLoginError()
                    return
                }
                self?.goToProductsVC()
            }
            
        }
    }
    
    private func goToProductsVC() {
        let vm = ProductViewModel()
        let vc = ProductsViewController(viewModel: vm)
        navigationController?.viewControllers = [vc]
    }
    
    private func showLoginError() {
        let alert = UIAlertController(title: "Lỗi", message: "vui lòng nhập email và password đúng để đăng nhập", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    @IBAction func didPressRegister(_ sender: UIButton) {
        let viewModel = RegisterViewModel()
        let vc = RegisterViewController(viewModel: viewModel)
        UIView.transition(with: (navigationController?.view)!, duration: 0.35, options: .transitionFlipFromLeft, animations: {
            self.navigationController?.pushViewController(vc, animated: false)
        }, completion: nil)
    }
    
}
