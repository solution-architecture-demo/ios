//
//  LoginViewModel.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 2/28/22.
//

import Foundation

class LoginViewModel {
    struct Credentials: Encodable {
        let email: String
        let password: String
    }
    
    func login(with credentials: Credentials, completion: @escaping ((Bool) -> Void)) {
        guard let url = URL(string: Router.login.url) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        let encoder = JSONEncoder()
        guard let dataBody = try? encoder.encode(credentials) else {
            completion(false)
            return
        }
        request.httpBody = dataBody
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                  let json = try? JSONSerialization.jsonObject(with: data) else { return }
            guard let token = (json as? [String: Any])?["token"] as? String else {
                completion(false)
                return
            }
            SessionManager.shared.token = token
            completion(true)
            
        }
        task.resume()
    }
}
