//
//  ProductCollectionViewCell.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 3/6/22.
//

import UIKit

struct Product {
    let imageUrl: String
    let name: String
    let description: String
    let price: Double
}

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func config(with product: Product) {
        imageView.loadImage(from: product.imageUrl)
        nameLabel.text = product.name
        priceLabel.text =  "$\(product.price)"
    }
}
