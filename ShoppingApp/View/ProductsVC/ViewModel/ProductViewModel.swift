//
//  ProductViewModel.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 3/6/22.
//

import Foundation

class ProductViewModel {
    private var products: [Product] = []
    
    func loadProduct(completion: @escaping (() -> Void)) {
        products = [
            Product(imageUrl: "https://product.hstatic.net/1000126467/product/coca-sleek-330ml-3_master.jpg", name: "coca", description: "mot", price: 20),
            Product(imageUrl: "https://product.hstatic.net/1000126467/product/coca-sleek-330ml-3_master.jpg", name: "coca", description: "mot", price: 20),
            Product(imageUrl: "https://product.hstatic.net/1000126467/product/coca-sleek-330ml-3_master.jpg", name: "coca", description: "mot", price: 20),
            Product(imageUrl: "https://product.hstatic.net/1000126467/product/coca-sleek-330ml-3_master.jpg", name: "coca", description: "mot", price: 20),
            Product(imageUrl: "https://product.hstatic.net/1000126467/product/coca-sleek-330ml-3_master.jpg", name: "coca", description: "mot", price: 20),
        ]
        completion()
    }
}

extension ProductViewModel {
    var numberOfItem: Int {
        products.count
    }
    
    func modelForCell(at indexPath: IndexPath) -> Product {
        products[indexPath.item]
    }
}
