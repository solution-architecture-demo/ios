//
//  RegisterViewController.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 2/27/22.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var address: UIView!
    
    let viewModel: RegisterViewModel
    
    init(viewModel: RegisterViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "RegisterViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func didPressRegister(_ sender: UIButton) {
        
        let credential = RegisterViewModel.Credentials(
            email: emailTextField.text ?? "",
            password: passwordTextField.text ?? "",
            confirmPassword: confirmPasswordTextField.text ?? "",
            address: addressTextField.text ?? ""
        )
        viewModel.register(with: credential) { [weak self] isSuccess in
            print("success")
        }
    }
    
    @IBAction func didPressLogin(_ sender: UIButton) {
        UIView.transition(with: (navigationController?.view)!, duration: 0.35, options: .transitionFlipFromRight, animations: {
            self.navigationController?.popViewController(animated: false)
        })
    }
    
}
