//
//  RegisterViewModel.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 2/28/22.
//

import Foundation
import CoreText

class RegisterViewModel {
    struct Credentials: Encodable {
        let email: String
        let password: String
        let confirmPassword: String
        let address: String
    }
    
    func register(with credentials: Credentials, completion: @escaping ((Bool) -> Void)) {
        guard let url = URL(string: Router.register.url) else {
            completion(false)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        let body = ["email": credentials.email, "password": credentials.password, "name": credentials.email]
        guard let bodyData = try? JSONEncoder().encode(body) else {
            completion(false)
            return
        }
        request.httpBody = bodyData
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                  let json = try? JSONSerialization.jsonObject(with: data),
            let status = (response as? HTTPURLResponse)?.statusCode else { return }
            completion(true)
            
        }
        task.resume()
    }
}
