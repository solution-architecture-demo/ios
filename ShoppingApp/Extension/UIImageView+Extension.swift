//
//  UIImageView+Extension.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 3/6/22.
//

import Foundation
import UIKit

extension UIImageView {
    func loadImage(from url: String) {
        DispatchQueue.global().async {
            guard let url = URL(string: url),
                  let data = try? Data(contentsOf: url) else { return }
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                self.image = image
            }
        }
    }
}
