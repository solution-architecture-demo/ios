//
//  Constant.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 2/28/22.
//

import Foundation

struct Constant {
    static let baseURL = "http://127.0.0.1:8000/api/"
}

enum Router: String {
    case login = "user/token/"
    case register = "user/create/"
    
    var url: String {
        Constant.baseURL + rawValue
    }
}
