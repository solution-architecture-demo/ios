//
//  SessionManager.swift
//  ShoppingApp
//
//  Created by DaiNLT.TCS on 2/28/22.
//

import Foundation

class SessionManager {
    static let shared = SessionManager()
    var token: String?
    private init() {}
}
